package bretzel.fr.uhc;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldBorder;

/**
 * Created by MrBretze on 01/01/2015.
 */

public class Game {

    private int maxX;
    private int maxY;
    private int maxZ;

    private int minX;
    private int minY;
    private int minZ;
    
    private World world = Bukkit.getWorld("game");
    private WorldBorder worldBorder = world.getWorldBorder();

    private Location center = worldBorder.getCenter();
    private double warningDistance = worldBorder.getWarningDistance();
    private States states;

    public boolean containtLocation(Location location) {
        for(int y = getMinY(); y <= getMaxY(); y++){
            for(int x = getMinX(); x <= getMaxX(); x++){
                for(int z = getMinZ(); z <= getMaxZ(); z++){
                    if((location.getBlockX() == x) && (location.getBlockY() == y) && (location.getBlockZ() == z)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private Location getCenter() {
        return center;
    }

    private WorldBorder getWorldBorder() {
        return worldBorder;
    }

    public World getWorld() {
        return world;
    }

    public double getWarningDistance() {
        return warningDistance;
    }

    public States getStates() {
        return states;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxZ() {
        return maxZ;
    }

    public int getMaxY() {
        return maxY;
    }

    public int getMinX() {
        return minX;
    }

    public int getMinY() {
        return minY;
    }

    public int getMinZ() {
        return minY;
    }

    public void setStates(States state) {
        this.states = state;
    }
}
