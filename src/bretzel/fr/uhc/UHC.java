package bretzel.fr.uhc;

import bretzel.fr.uhc.events.PlayerListener;
import bretzel.fr.uhc.utils.InComingPluginChanel;
import bretzel.fr.uhc.utils.RestartServer;
import bretzel.fr.uhc.world.BiomeHandlers;
import bretzel.fr.uhc.world.Biomes;

import com.google.common.collect.Lists;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 * Created by MrBretze on 01/01/2015.
 */

public class UHC extends JavaPlugin {

    private static UHC instance;
    private PluginManager pluginManager = Bukkit.getPluginManager();
    private List<UUID> pLists = Lists.newLinkedList();
    private Game game = new Game();
    private BiomeHandlers biomeHandlers = new BiomeHandlers();

    public static UHC getInstance() {
        return instance;
    }

    public List<UUID> getPlayerList() {
        return pLists;
    }

    public Game getGame() {
        return game;
    }

    public BiomeHandlers getBiomeHandlers() {
        return biomeHandlers;
    }

    public PluginManager getPluginManager() {
        return pluginManager;
    }

    public void restartServer(File file) {
        RestartServer server = new RestartServer(file.toString());
        server.Restart();
    }

    @Override
    public void onEnable() {
        try {
            Class.forName("org.bukkit.WorldBorder;"); // test in 1.8 bukkit version
        } catch (Exception e) {
            setEnabled(false);
        }

        instance = this;

        getPluginManager().registerEvents(new PlayerListener(), UHC.getInstance());

        getBiomeHandlers().swapBiome(Biomes.BEACH, Biomes.FLOWERS_FOREST);
        getBiomeHandlers().swapBiome(Biomes.OCEAN, Biomes.PLAINS);
        getBiomeHandlers().swapBiome(Biomes.DEEP_OCEAN, Biomes.FOREST);

        getGame().setStates(States.LOBBY);

        getInstance().getServer().getMessenger().registerOutgoingPluginChannel(getInstance(), "BungeeCord");
        getInstance().getServer().getMessenger().registerIncomingPluginChannel(getInstance(), "BungeeCord", new InComingPluginChanel());
    }

    @Override
    public void onDisable() {

    }
}
