package bretzel.fr.uhc.events;

import bretzel.fr.uhc.States;
import bretzel.fr.uhc.UHC;
import bretzel.fr.uhc.utils.BungeeUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by MrBretze on 01/01/2015.
 */

public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if(UHC.getInstance().getGame().getStates() == States.LOBBY) {
            Location location = new Location(event.getPlayer().getWorld(), 0.0, 100.0, 0.0);
            if (event.getPlayer() != null) {
                Player player = event.getPlayer();
                if (!UHC.getInstance().getPlayerList().contains(player.getUniqueId())) {
                    UHC.getInstance().getPlayerList().add(player.getUniqueId());
                }
                player.teleport(location);
                event.setJoinMessage(null);
                Bukkit.broadcastMessage(ChatColor.GREEN + player.getDisplayName() + "has join the UHC game !");
            }
        } else {
            BungeeUtils.playerSendToServer(event.getPlayer(), "lobby");
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if(UHC.getInstance().getGame().getStates() == States.LOBBY) {
            Player player = event.getPlayer();
            if(UHC.getInstance().getPlayerList().contains(player.getUniqueId())) {
                UHC.getInstance().getPlayerList().remove(player.getUniqueId());
            }
        } else {

        }
    }
}
