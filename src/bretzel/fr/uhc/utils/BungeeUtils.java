package bretzel.fr.uhc.utils;

import bretzel.fr.uhc.UHC;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

/**
 * Created by MrBretze on 01/01/2015.
 */

public class BungeeUtils {

    public static void playerSendToServer(Player player, String server) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("Connect");
            out.writeUTF(server);
        } catch (Exception e) {
            e.printStackTrace();
        }
        player.sendPluginMessage(UHC.getInstance(), "BungeeCord", b.toByteArray());
    }
}
