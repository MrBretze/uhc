package bretzel.fr.uhc.utils;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

/**
 * Created by MrBretze on 02/01/2015.
 */

public class InComingPluginChanel implements PluginMessageListener {

    private static String[] servers;

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        ByteArrayInputStream i = new ByteArrayInputStream(message);
        DataInputStream in = new DataInputStream(i);

        String subchannel;
        try {
            subchannel = in.readUTF();
            if (subchannel.equalsIgnoreCase("GetServers")) {
                String[] serverList = in.readUTF().split(", ");
                servers = serverList;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return servers get all servers
     */
    public static String[] getServers() {
        return servers;
    }
}
