package bretzel.fr.uhc.utils;

import bretzel.fr.uhc.UHC;
import net.minecraft.server.v1_8_R1.MinecraftServer;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

/**
 * Created by MrBretze on 02/01/2015.
 */

public class RestartServer {

    private String command = "";

    public RestartServer(String comd) {
        setCommand(comd);
    }

    public void Restart() {
        System.out.print("Starting restarting server.");
        try {
            Thread.sleep(100);
            Iterator<UUID> players = UHC.getInstance().getPlayerList().iterator();

            while (players.hasNext()) {
                Player p = Bukkit.getPlayer(players.next());

                if(p.isOnline()) {
                    p.kickPlayer(ChatColor.RED + "Restarting server.");
                }
            }
            Thread.sleep(100);

            MinecraftServer.getServer().getServerConnection().b();

            Thread.sleep(100);

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    String sys = System.getProperty("os.name");
                    if(sys.contains("win")) {
                        try {
                            Runtime.getRuntime().exec(getCommand());
                        }catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Runtime.getRuntime().exec(new String[]{"sh", getCommand()});
                        }catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            t.setDaemon(true);

            Thread.sleep(1000); // 1000 = 1sec

            clearMonde();

            Thread.sleep(100);
            Runtime.getRuntime().addShutdownHook(t);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void clearMonde() throws IOException {
        FileUtils.deleteDirectory(getGameWorld().getWorldFolder());
    }

    public World getGameWorld() {
        return Bukkit.getWorld("game");
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getCommand(){
        return command;
    }
}
