package bretzel.fr.uhc.world;

import java.lang.reflect.Field;
import java.util.Arrays;

import net.minecraft.server.v1_8_R1.BiomeBase;

import org.bukkit.Bukkit;

/**
 * Created by MrBretze on 02/01/2015.
 */

public class BiomeHandlers {
    private BiomeBase[] origBiomes;

    public BiomeHandlers() {
        this.origBiomes = getMcBiomesCopy();
    }

    public void swapBiome(Biomes oldBiome, Biomes newBiome) {
        if (oldBiome.getId() != Biomes.SKY.getId()) {
            BiomeBase[] biomes = getMcBiomes();
            biomes[oldBiome.getId()] = getOrigBiome(newBiome.getId());
        } else {
            Bukkit.getLogger().warning("Cannot swap SKY biome!");
        }
    }

    private BiomeBase[] getMcBiomesCopy() {
        BiomeBase[] b = getMcBiomes();
        return (BiomeBase[]) Arrays.copyOf(b, b.length);
    }

    private BiomeBase[] getMcBiomes() {
        try {
            Field biomeF = BiomeBase.class.getDeclaredField("biomes");
            biomeF.setAccessible(true);
            return (BiomeBase[]) biomeF.get(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new BiomeBase[256];
    }

    private BiomeBase getOrigBiome(int value) {
        return this.origBiomes[value];
    }
}
